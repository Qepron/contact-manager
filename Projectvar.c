#include <gtk/gtk.h>

/**
 * Ui global pointers
**/
GtkWindow *window;
GtkListStore *contact_list;
GtkTreeView *treeview;
GtkTreeSelection *treeselection;
GtkTreeViewColumn *cx1;
GtkTreeViewColumn *cx2;
GtkTreeViewColumn *cx3;
GtkCellRenderer *cr1;
GtkCellRenderer *cr2;
GtkCellRenderer *cr3;
GtkBuilder *builder;
GtkWidget *add_button, *delete_button, *show_button;
GtkSearchEntry *searchentry;
GtkLabel *nb_contacts;
GtkDialog *add_dialog;

/**
 * Back global pointers
**/
GHashTable *contact_hashtable;

/**
 * Struct for the contacts
**/
typedef struct Contact_t
{
    gchar *nom;
    gchar *prenom;
    gchar *adresse;
    gchar *e_mail;
    gchar *code_postal;
    gchar *num1;
    gchar *num2;
    gchar *num3;
    gchar *type;
} Contact_t;

/**
 * Struct for the dialog entrys
**/
typedef struct Entrys_t
{
    GtkEntry *nom_entry;
    GtkEntry *prenom_entry;
    GtkEntry *adresse_entry;
    GtkEntry *e_mail_entry;
    GtkEntry *code_postal_entry;
    GtkEntry *num1_entry;
    GtkEntry *num2_entry;
    GtkEntry *num3_entry;
    GtkEntry *combo_entry;
} Entrys_t;

//! Régler nombre de contact après modification

/**
 * Update nb contact label function
**/
void update_nb_contacts()
{
    // Update the label which display the amount of contacts
    gchar *nbc = g_strdup_printf("%i Contacts", g_hash_table_size(contact_hashtable));
    gtk_label_set_text(nb_contacts, nbc);
}

/**
 * Entrys functions
**/
Contact_t *get_entry(Entrys_t *p_entrys)
{
    /**
     * Get text from entrys
    **/
    gchar *nom = g_strdup(gtk_entry_get_text(p_entrys->nom_entry));
    gchar *prenom = g_strdup(gtk_entry_get_text(p_entrys->prenom_entry));
    gchar *adresse = g_strdup(gtk_entry_get_text(p_entrys->adresse_entry));
    gchar *e_mail = g_strdup(gtk_entry_get_text(p_entrys->e_mail_entry));
    gchar *code_postal = g_strdup(gtk_entry_get_text(p_entrys->code_postal_entry));
    gchar *num1 = g_strdup(gtk_entry_get_text(p_entrys->num1_entry));
    gchar *num2 = g_strdup(gtk_entry_get_text(p_entrys->num2_entry));
    gchar *num3 = g_strdup(gtk_entry_get_text(p_entrys->num3_entry));
    gchar *type = g_strdup(gtk_entry_get_text(p_entrys->combo_entry));

    /**
     * Create a new contact from the texts gets
    **/
    Contact_t *p_contact = g_try_malloc(sizeof(Contact_t));
    p_contact->nom = nom;
    p_contact->prenom = prenom;
    p_contact->adresse = adresse;
    p_contact->e_mail = e_mail;
    p_contact->code_postal = code_postal;
    p_contact->num1 = num1;
    p_contact->num2 = num2;
    p_contact->num3 = num3;
    p_contact->type = type;
    return p_contact;
}

void reset_entrys(Entrys_t *p_entrys)
{
    /**
     * Reset the entrys to none
    **/
    gtk_entry_set_text(p_entrys->nom_entry, "");
    gtk_entry_set_text(p_entrys->prenom_entry, "");
    gtk_entry_set_text(p_entrys->adresse_entry, "");
    gtk_entry_set_text(p_entrys->e_mail_entry, "");
    gtk_entry_set_text(p_entrys->code_postal_entry, "");
    gtk_entry_set_text(p_entrys->num1_entry, "");
    gtk_entry_set_text(p_entrys->num2_entry, "");
    gtk_entry_set_text(p_entrys->num3_entry, "");
    gtk_entry_set_text(p_entrys->combo_entry, "");
}

/**
 * Remove contact functions
**/
gboolean is_selected(Contact_t *p_contact)
{
    /**
     * Initialise needed instances
    **/
    GtkTreeModel *model;
    model = gtk_tree_view_get_model(treeview);
    GtkTreeIter iter;
    // Key to find the contact to remove
    gchar *selected_num;

    // Initialise the tree iter...
    if (gtk_tree_model_get_iter_first(model, &iter) == FALSE)
    {
        return 0;
    }
    // Check whether a row is select...
    if (gtk_tree_selection_get_selected(treeselection, &model, &iter))
    {
        // Get the num of the contact in the row selected to pass as key in the hashtable
        gtk_tree_model_get(model, &iter, 2, &selected_num, -1);
        if (g_strcmp0(p_contact->num1, selected_num) == 0)
        {
            return 1;
        }
        g_free(selected_num);
    }
    return 0;
}

/**
 * Add contact function
**/
void add_contact(Contact_t *p_contact)
{
    GtkTreeIter iter;

    // Add and set a new row
    gtk_list_store_append(contact_list, &iter);
    gtk_list_store_set(contact_list, &iter, 0, p_contact->nom, 1, p_contact->prenom, 2, p_contact->num1, -1);
    // Insert a newly created contact
    g_hash_table_insert(contact_hashtable, p_contact->num1, p_contact);
}

/**
 * Dialog functions
**/
guint open_dialog_warning(guint state)
{
    GtkDialog *edit_warning_dialog = GTK_DIALOG(gtk_builder_get_object(builder, "edit_warning_dialog"));
    GtkDialog *add_warning_dialog = GTK_DIALOG(gtk_builder_get_object(builder, "add_warning_dialog"));
    GtkDialog *supp_warning_dialog = GTK_DIALOG(gtk_builder_get_object(builder, "supp_warning_dialog"));

    gint edit_state;
    gint add_state;
    gint supp_state;

    switch (state)
    {
    case 0:
        edit_state = gtk_dialog_run(GTK_DIALOG(edit_warning_dialog));
        // Handle the state of the dialog...
        switch (edit_state)
        {
        default:
            break;
        }
        gtk_widget_hide(GTK_WIDGET(edit_warning_dialog));
        break;
    case 1:
        add_state = gtk_dialog_run(GTK_DIALOG(add_warning_dialog));
        // Handle the state of the dialog...
        switch (add_state)
        {
        case GTK_RESPONSE_YES:
            gtk_widget_hide(GTK_WIDGET(add_warning_dialog));
            return 1;
            break;
        case GTK_RESPONSE_NO:
            gtk_widget_hide(GTK_WIDGET(add_warning_dialog));
            return 0;
            break;
        default:
            break;
        }
        gtk_widget_hide(GTK_WIDGET(add_warning_dialog));
        break;
    case 2:
        supp_state = gtk_dialog_run(GTK_DIALOG(supp_warning_dialog));
        // Handle the state of the dialog...
        switch (supp_state)
        {
        case GTK_RESPONSE_YES:
            gtk_widget_hide(GTK_WIDGET(supp_warning_dialog));
            return 1;
            break;
        case GTK_RESPONSE_NO:
            gtk_widget_hide(GTK_WIDGET(supp_warning_dialog));
            return 0;
            break;
        default:
            break;
        }
        gtk_widget_hide(GTK_WIDGET(supp_warning_dialog));
        break;
    default:
        break;
    }
    return 0;
}

void remove_contact_from_button(guint tag)
{
    /**
     * Initialise needed instances
    **/
    GtkListStore *liststore;
    liststore = GTK_LIST_STORE(gtk_tree_view_get_model(treeview));
    GtkTreeModel *model;
    model = gtk_tree_view_get_model(treeview);
    GtkTreeIter iter;
    // Key to find the contact to remove
    gchar *selected_num;

    // Initialise the tree iter...
    if (gtk_tree_model_get_iter_first(model, &iter) == FALSE)
    {
        return;
    }
    // Check whether a row is select...
    if (gtk_tree_selection_get_selected(treeselection, &model, &iter))
    {
        // Get the num of the contact in the row selected to pass as key in the hashtable
        gtk_tree_model_get(model, &iter, 2, &selected_num, -1);
        if (tag != 1) {
            if (!open_dialog_warning(2)) {
                return;
            }
        } 
        // Remove the contact from the hashtable and its assiciated row in the tree view
        g_hash_table_remove(contact_hashtable, selected_num);
        gtk_list_store_remove(liststore, &iter);
        update_nb_contacts();
        g_free(selected_num);
    }
}

void remove_contact_from_contact(Contact_t *p_contact)
{
    GtkListStore *liststore;
    liststore = GTK_LIST_STORE(gtk_tree_view_get_model(treeview));
    gchar *s = p_contact->num1;
    GList *contactlist = g_hash_table_get_values(contact_hashtable);
    guint i = 0;
    guint j = 0;
    GtkTreeIter iter;
    gchar *selected_num;
    GtkTreeModel *model;
    model = gtk_tree_view_get_model(treeview);
    for (GList *c = contactlist; c != NULL; c = c->next)
    {
        Contact_t *contact = c->data;
        if (g_strcmp0(s, contact->num1) == 0)
        {
            i += 1;
        }
    }
    if (i == 1)
    {
        while (j < g_hash_table_size(contact_hashtable))
        {
            gtk_tree_model_get_iter_from_string(model, &iter, g_strdup_printf("%i", j));
            gtk_tree_model_get(model, &iter, 2, &selected_num, -1);

            if (g_strcmp0(s, selected_num) == 0)
            {
                break;
            }
            j++;
        }
        // Truc a faire
        gtk_list_store_remove(liststore, &iter);
        g_free(selected_num);
    }
}



void open_dialog_add(Contact_t *p_contact)
{
    /**
     * Dialog initialisation
    **/
    add_dialog = GTK_DIALOG(gtk_builder_get_object(builder, "add_dialog"));

    /**
     * Entrys initialisation
    **/
    GtkEntry *nom_entry = GTK_ENTRY(gtk_builder_get_object(builder, "nom_entry"));
    GtkEntry *prenom_entry = GTK_ENTRY(gtk_builder_get_object(builder, "prenom_entry"));
    GtkEntry *adresse_entry = GTK_ENTRY(gtk_builder_get_object(builder, "adresse_entry"));
    GtkEntry *e_mail_entry = GTK_ENTRY(gtk_builder_get_object(builder, "e_mail_entry"));
    GtkEntry *code_postal_entry = GTK_ENTRY(gtk_builder_get_object(builder, "code_postal_entry"));
    GtkEntry *num1_entry = GTK_ENTRY(gtk_builder_get_object(builder, "num1_entry"));
    GtkEntry *num2_entry = GTK_ENTRY(gtk_builder_get_object(builder, "num2_entry"));
    GtkEntry *num3_entry = GTK_ENTRY(gtk_builder_get_object(builder, "num3_entry"));
    GtkEntry *combo_entry = GTK_ENTRY(gtk_builder_get_object(builder, "combo_entry"));
    // Entry parameter
    Entrys_t entrys = {nom_entry,
                       prenom_entry,
                       adresse_entry,
                       e_mail_entry,
                       code_postal_entry,
                       num1_entry,
                       num2_entry,
                       num3_entry,
                       combo_entry};
    Entrys_t *p_entrys = &entrys;
    // Contact meant to be created from user inputs
    Contact_t *p_c = g_try_malloc(sizeof(Contact_t));

    /**
     * Edit state initialisation
    **/
    gboolean edit_state = g_hash_table_contains(contact_hashtable, p_contact->num1);
    if (edit_state)
    {
        gtk_entry_set_text(p_entrys->nom_entry, p_contact->nom);
        gtk_entry_set_text(p_entrys->prenom_entry, p_contact->prenom);
        gtk_entry_set_text(p_entrys->adresse_entry, p_contact->adresse);
        gtk_entry_set_text(p_entrys->e_mail_entry, p_contact->e_mail);
        gtk_entry_set_text(p_entrys->code_postal_entry, p_contact->code_postal);
        gtk_entry_set_text(p_entrys->num1_entry, p_contact->num1);
        gtk_entry_set_text(p_entrys->num2_entry, p_contact->num2);
        gtk_entry_set_text(p_entrys->num3_entry, p_contact->num3);
        gtk_entry_set_text(p_entrys->combo_entry, p_contact->type);
    }

    /**
     * Dialog state initialisation
    **/
    gint state = gtk_dialog_run(GTK_DIALOG(add_dialog));
    // Handle the state of the dialog...
    switch (state)
    {
    case GTK_RESPONSE_APPLY: // Show a message if there is no number //! 6.2
        // get text from entrys...
        p_c = get_entry(p_entrys);
        if (strlen(p_c->num1) < 1 || (strlen(p_c->nom) < 1 || strlen(p_c->prenom) < 1))
        {
            open_dialog_warning(0);
            open_dialog_add(p_contact);
            break;
        }
        // Edit mode...
        if (edit_state)
        {
            if (!g_hash_table_contains(contact_hashtable, p_c->num1)) // Pas présent
            {
                add_contact(p_c);
            }
            else
            {
                // check si numéro selectionné ou pas
                if (!is_selected(p_contact))
                {
                    g_print("Hey !");
                    if (!open_dialog_warning(1)) // Présent
                    {
                        open_dialog_add(p_contact);
                    }
                    // remove_contact_from_button();
                }
                remove_contact_from_contact(p_c);
                add_contact(p_c);
            }
            remove_contact_from_button(1);

            if (p_c->num1 != p_contact->num1)
            {
                update_nb_contacts();
            }
        }
        else
        {
            if (!g_hash_table_contains(contact_hashtable, p_c->num1)) // Pas présent
            {
                add_contact(p_c);
            }
            else // Présent
            {
                if (open_dialog_warning(1))
                {
                    remove_contact_from_contact(p_c);
                    add_contact(p_c);
                }
                else
                {
                    open_dialog_add(p_contact);
                }
            }
            update_nb_contacts();
        }
        reset_entrys(p_entrys);
        break;
    case GTK_RESPONSE_CANCEL:
        reset_entrys(p_entrys);
        break;
    default:
        break;
    }
    gtk_widget_hide(GTK_WIDGET(add_dialog));
    // g_free(p_c);
}

void open_dialog_edit(Contact_t *contact)
{
    /**
     * Dialog initialisation
    **/
    GtkDialog *show_dialog = GTK_DIALOG(gtk_builder_get_object(builder, "show_dialog"));

    /**
     * Dialog initialisation
    **/
    GtkLabel *nom_lab = GTK_LABEL(gtk_builder_get_object(builder, "nom_lab"));
    GtkLabel *prenom_lab = GTK_LABEL(gtk_builder_get_object(builder, "prenom_lab"));
    GtkLabel *adresse_lab = GTK_LABEL(gtk_builder_get_object(builder, "adresse_lab"));
    GtkLabel *e_mail_lab = GTK_LABEL(gtk_builder_get_object(builder, "e_mail_lab"));
    GtkLabel *code_postal_lab = GTK_LABEL(gtk_builder_get_object(builder, "code_postal_lab"));
    GtkLabel *num1_lab = GTK_LABEL(gtk_builder_get_object(builder, "num1_lab"));
    GtkLabel *num2_lab = GTK_LABEL(gtk_builder_get_object(builder, "num2_lab"));
    GtkLabel *num3_lab = GTK_LABEL(gtk_builder_get_object(builder, "num3_lab"));
    GtkLabel *type_lab = GTK_LABEL(gtk_builder_get_object(builder, "type_lab"));

    gtk_label_set_text(nom_lab, contact->nom);
    gtk_label_set_text(prenom_lab, contact->prenom);
    gtk_label_set_text(adresse_lab, contact->adresse);
    gtk_label_set_text(e_mail_lab, contact->e_mail);
    gtk_label_set_text(code_postal_lab, contact->code_postal);
    gtk_label_set_text(num1_lab, contact->num1);
    gtk_label_set_text(num2_lab, contact->num2);
    gtk_label_set_text(num3_lab, contact->num3);
    gtk_label_set_text(type_lab, contact->type);

    gint state = gtk_dialog_run(GTK_DIALOG(show_dialog));
    // Handle the state of the dialog...
    switch (state)
    {
    case GTK_RESPONSE_APPLY:
        open_dialog_add(contact);
        break;
    default:
        break;
    }
    gtk_widget_hide(GTK_WIDGET(show_dialog));
}

/**
 * Main window widget related functions
**/
void show_contact()
{
    /**
     * Initialise needed instances
    **/
    GtkTreeModel *model;
    model = gtk_tree_view_get_model(treeview);
    GtkTreeIter iter;
    // Key to find the contact to remove
    gchar *selected_num;
    // Contact to return
    Contact_t *contact = g_try_malloc(sizeof(Contact_t));

    // Initialise the tree iter...
    if (gtk_tree_model_get_iter_first(model, &iter) == FALSE)
    {
        return;
    }
    // Check whether a row is select...
    if (gtk_tree_selection_get_selected(treeselection, &model, &iter))
    {
        // Get the num of the contact in the row selected to pass as key in the hashtable
        gtk_tree_model_get(model, &iter, 2, &selected_num, -1);
        contact = g_hash_table_lookup(contact_hashtable, selected_num);
        open_dialog_edit(contact);
    }
    // g_free(contact);
}

void search(GtkWidget *search_entry)
{
    gchar *s = g_strdup(gtk_entry_get_text(GTK_ENTRY(search_entry)));
    GList *contactlist = g_hash_table_get_values(contact_hashtable);
    guint s_len = strlen(s);
    guint i = 0;
    guint j = 0;
    GtkTreeIter iter;
    gchar *selected_nom;
    gchar *selected_prenom;
    gchar *selected_num;
    GtkTreeModel *model;
    model = gtk_tree_view_get_model(treeview);
    for (GList *c = contactlist; c != NULL; c = c->next)
    {
        Contact_t *contact = c->data;
        if (g_ascii_strncasecmp(s, contact->nom, s_len) == 0 ||
            g_ascii_strncasecmp(s, contact->prenom, s_len) == 0 ||
            g_ascii_strncasecmp(s, contact->num1, s_len) == 0 ||
            g_ascii_strncasecmp(s, contact->num2, s_len) == 0 ||
            g_ascii_strncasecmp(s, contact->num3, s_len) == 0)
        {
            i += 1;
        }
    }
    if (i == 1)
    {
        while (j < g_hash_table_size(contact_hashtable))
        {
            gtk_tree_model_get_iter_from_string(model, &iter, g_strdup_printf("%i", j));
            gtk_tree_model_get(model, &iter, 0, &selected_nom, -1);
            gtk_tree_model_get(model, &iter, 1, &selected_prenom, -1);
            gtk_tree_model_get(model, &iter, 2, &selected_num, -1);

            if (g_ascii_strncasecmp(s, selected_nom, s_len) == 0 ||
                g_ascii_strncasecmp(s, selected_prenom, s_len) == 0 ||
                g_ascii_strncasecmp(s, selected_num, s_len) == 0)
            {
                break;
            }
            j++;
        }
        gtk_tree_selection_select_iter(treeselection, &iter);
        g_free(selected_nom);
        g_free(selected_prenom);
        g_free(selected_num);
    }
}

int main(void)
{
    /* Initialisation de gtk */
    gtk_init(NULL, NULL);

    /**
     * Builder initialisation
    **/
    builder = gtk_builder_new_from_file("Projet.glade");
    gtk_builder_connect_signals(builder, NULL);

    /**
     * Window initialisation
    **/
    window = GTK_WINDOW(gtk_builder_get_object(builder, "window"));
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
    gtk_window_set_title(window, "Gestionnaire de contacts");

    /**
     * Widget initialisation
    **/
    searchentry = GTK_SEARCH_ENTRY(gtk_builder_get_object(builder, "search_entry"));
    g_signal_connect(searchentry, "search-changed", G_CALLBACK(search), NULL);

    treeview = GTK_TREE_VIEW(gtk_builder_get_object(builder, "contact_treeview"));
    treeselection = GTK_TREE_SELECTION(gtk_builder_get_object(builder, "tree_selection"));

    add_button = GTK_WIDGET(gtk_builder_get_object(builder, "add_button"));
    g_signal_connect(add_button, "clicked", G_CALLBACK(open_dialog_add), NULL);

    delete_button = GTK_WIDGET(gtk_builder_get_object(builder, "delete_button"));
    g_signal_connect(delete_button, "clicked", G_CALLBACK(remove_contact_from_button), NULL);

    show_button = GTK_WIDGET(gtk_builder_get_object(builder, "show_button"));
    g_signal_connect(show_button, "clicked", G_CALLBACK(show_contact), NULL);

    nb_contacts = GTK_LABEL(gtk_builder_get_object(builder, "nb_contacts"));

    /**
     * Liste store initialisation
    **/
    contact_list = GTK_LIST_STORE(gtk_builder_get_object(builder, "contact_list"));

    /**
     * Column initialisation
    **/
    cx1 = GTK_TREE_VIEW_COLUMN(gtk_builder_get_object(builder, "cx1"));
    cx2 = GTK_TREE_VIEW_COLUMN(gtk_builder_get_object(builder, "cx2"));
    cx3 = GTK_TREE_VIEW_COLUMN(gtk_builder_get_object(builder, "cx3"));
    cr1 = GTK_CELL_RENDERER(gtk_builder_get_object(builder, "cr1"));
    cr2 = GTK_CELL_RENDERER(gtk_builder_get_object(builder, "cr2"));
    cr3 = GTK_CELL_RENDERER(gtk_builder_get_object(builder, "cr3"));
    gtk_tree_view_column_add_attribute(cx1, cr1, "text", 0);
    gtk_tree_view_column_add_attribute(cx2, cr2, "text", 1);
    gtk_tree_view_column_add_attribute(cx3, cr3, "text", 2);

    /**
     * Contact storage initialisation
    **/
    contact_hashtable = g_hash_table_new(g_str_hash, g_str_equal);

    /* Affichage de la fenêtre */
    gtk_widget_show_all(GTK_WIDGET(window));
    /* Lancement de la main loop */
    gtk_main();

    return EXIT_SUCCESS;
}